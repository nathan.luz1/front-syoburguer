const container = document.getElementById('container');
const registerBtn = document.getElementById('register');
const loginBtn = document.getElementById('login');
const connectBtn = document.getElementById('connect');

registerBtn.addEventListener('click', () => {
    container.classList.add("active");

});

loginBtn.addEventListener('click', () => {
    container.classList.remove("active");

});

function register() {
    event.preventDefault();

    const nome = document.getElementById("nome").value;
    const email = document.getElementById("email").value;
    const senha = document.getElementById("senha").value;
    const cpf = document.getElementById("cpf").value;

    fetch(
        "http://127.0.0.1:9090/api/usuario",
        {
            headers: {
                "Accept": "application/json",
                "Content-Type": "application/json"
            },
            method: "POST",
            body: JSON.stringify({"email": email, "nome": nome, "cpf": cpf, "senha": senha, "permissao": 1})

        }
    )
    .then(function(resposta) {
        alert("Registrado com sucesso!")
        window.location.href = 'file:///home/syonet/Documentos/PP/DevClass23/exercicios-front/final/front-syoburguer/syoburguer-preview/Projeto-SyoBurguer-main/index.html';

    })
    .catch(function(resposta){
        alert("Ocorreu um erro ao cadastrar! Verifique os dados inseridos no campo");

    })

}

function login() {
    event.preventDefault();

    const email = document.getElementById("emailLg").value;
    const senha = document.getElementById("senhaLg").value;

    fetch(`http://127.0.0.1:9090/api/usuario/find?email=${encodeURIComponent(email)}&senha=${encodeURIComponent(senha)}`, {
        headers: {
            "Accept": "application/json",
            "Content-Type": "application/json"
        },
        method: "GET"
    })
    .then(function(resposta) {
        if (resposta.ok) {
            alert("Login realizado com sucesso!");
            window.location.href = "file:///home/syonet/Documentos/PP/DevClass23/exercicios-front/final/front-syoburguer/syoburguer-preview/Projeto-SyoBurguer-main/index.html";

        } else if (resposta.status === 401) {
            alert("Credenciais inválidas! Verifique seu email e senha!");

        } else if (resposta.status === 500) {
            alert("Usuário não cadastrado!");

        }
         else {
            throw new Error(`Erro na solicitação: ${resposta.status} - ${resposta.statusText}`);

        }
    })
    .catch(function(erro) {
        alert(`Ocorreu um erro na solicitação: ${erro.message}`);

    });

}