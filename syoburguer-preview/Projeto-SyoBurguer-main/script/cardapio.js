let cartTotal = 0;
let cartTotalPrice;

document.addEventListener('DOMContentLoaded', function() {
    let list = document.querySelectorAll('.list .item');
    cartTotalPrice = document.querySelector('.cart-total-price');

    list.forEach(item => {
        let countInput = item.querySelector('#value');
        let plusButton = item.querySelector('#plus');
        let minusButton = item.querySelector('#minus');

        plusButton.addEventListener('click', function() {
            let count = parseInt(countInput.textContent);
            countInput.textContent = count + 1;


        });

        minusButton.addEventListener('click', function() {
            let count = parseInt(countInput.textContent);
            if (count > 1 ) {
                countInput.textContent = count - 1;

            }
        });

        item.addEventListener('click', function(event) {
            if (event.target.classList.contains('add')) {
                var itemNew = item.cloneNode(true);
                let checkIsset = false;

                let listCart = document.querySelectorAll('.cart .item');
                listCart.forEach(cart => {
                    if (cart.getAttribute('data-key') == itemNew.getAttribute('data-key')) {
                        checkIsset = true;
                        cart.classList.add('danger');
                        setTimeout(function() {
                            cart.classList.remove('danger');
                        }, 1000);
                    }
                });

                if (checkIsset == false) {
                    document.querySelector('.listCart').appendChild(itemNew);
                    updateCartTotal(item);
                }
            }
        });
    });

});

function updateCartTotal(item) {
    let itemPrice = parseFloat(item.getAttribute('data-price'));
    let itemCount = parseInt(item.querySelector('#value').textContent);
    cartTotal += itemPrice * itemCount;   

    cartTotalPrice.textContent = `R$ ${cartTotal.toFixed(2)}`;

}

function removeItem(key) {
    let listCart = document.querySelectorAll('.cart .item');
    listCart.forEach(item => {
        if (item.getAttribute('data-key') == key) {
           let itemPrice = parseFloat(item.getAttribute('data-price'));
           let itemCount = parseInt(item.querySelector('#value').textContent);
            cartTotal -= itemPrice * itemCount;
            
            cartTotalPrice.textContent = `R$ ${cartTotal.toFixed(2)}`;

            item.remove();
            return;

        }

    });
}

function confirmar() {
    alert("Em breve!")
    
}